// Sanitize the data from the API.  It will remove the comma and convert strings into number

export default function toNum(str){
    // Ex: "25, 222, 223"
    // convert string to array to gain access to array methods by using the spread operator
    const arr = [...str]
    // if we console this, console.log(arr) ["2", "5", ",", "2",... ]
    const filteredArr = arr.filter(element => element !== ",")
    // we filter out the commas in the string
    return parseInt(filteredArr.reduce((x,y) => x + y))
    // reduce the filtered array back to a single string without the commas


}