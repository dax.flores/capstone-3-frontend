import React from 'react';
import { Profile } from '../../components/Profile';
import Head from 'next/head';

function index() {
    return (
        <React.Fragment>
        <Head>
            <title>Profile</title>
        </Head>
           <Profile />
        </React.Fragment>
    );
}

export default index;
