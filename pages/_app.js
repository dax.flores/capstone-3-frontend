import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import NaviBar from '../components/NaviBar';
import { UserProvider } from '../UserContext';

import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';

function MyApp({ Component, pageProps }) {



	// State hook for the user details
	const [user, setUser] = useState({
		token: null
	})

	// Function for clearing the localStorage
	const unsetUser = () => {

		localStorage.clear();

		// Changes the value of the user state back to it's original value
		setUser({ 
			token: null
		});

	}

	useEffect(() => {
   		setUser({
   			token: localStorage.getItem('token'),
   		})
	}, [])

	return (
  		<React.Fragment>
  			<UserProvider value={{user, setUser, unsetUser}}>
  				<NaviBar />
						
  				<Container className="my-5">
  					<Component {...pageProps} />
  				</Container>
  			</UserProvider>	
  		</React.Fragment>
  	)
}

export default MyApp
