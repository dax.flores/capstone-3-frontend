import React from 'react';
import { Records } from '../../components/Records';
import Head from 'next/head';

function index() {
	return (
		<React.Fragment>
			<Head>
				<title>Records</title>
			</Head>
			<Records />
		</React.Fragment>
	)
}
export default index;
