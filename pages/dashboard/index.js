import React from 'react';
import Head from 'next/head';
import { Jumbotron, Container, Row, Col} from 'react-bootstrap'
import { Profile } from '../../components/Profile';
import { RecordCards } from '../../components/RecordCards';
import { AddTransaction } from '../../components/AddTransaction';
import { CreateCategory } from '../../components/CreateCategory';
import { Figures } from '../../components/Figures';






function index () {

    return (
        <React.Fragment>
            <Head>
                <title>Dashboard</title>
            </Head>
            <Jumbotron fluid style={{ backgroundColor: '#F5FCFF' }}>
                <Profile />
                <Figures />
                <RecordCards />
            </Jumbotron>
              <Container fluid>
              <Row>
                <Col>
                        <CreateCategory />
                </Col>
                <Col>
                        <AddTransaction />
                </Col>
              </Row>
              </Container>
        </React.Fragment>
    );
}

export default index;
