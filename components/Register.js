import React, { useState, useEffect } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import Link from 'next/link';
import { Form, Button } from 'react-bootstrap';
import AppHelper from '../app-helper'


export default function Register() {


    const [email, setEmail] = useState('');
    const [firstName, setfirstName] = useState('');
    const [lastName, setlastName] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [isActive, setIsActive] = useState(false);


    /* console.log(firstName)
    console.log(lastName)
    console.log(email);
    console.log(password1);
    console.log(password2); */

    function registerUser(e) {


        e.preventDefault();

        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password1
            })
        }
        fetch(`${AppHelper.API_URL}/users/`, options)
            .then(AppHelper.toJSON)
            .then(data => {
                console.log(data);

                if (data === true) {

                    alert("Registered Successfully");
                    console.log("Thank you for registering!")

                    Router.push('/login');

                } else {

                    alert("Something went wrong.")

                }


            })


        setfirstName("");
        setlastName("");
        setEmail("");
        setPassword1("");
        setPassword2("");

    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if ((firstName !== "" && lastName !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [firstName, lastName, email, password1, password2])

    return (
        <React.Fragment>
            <Head>
                <title>Register</title>
            </Head>
            <Form onSubmit={(e) => registerUser(e)}>
                <Form.Group controlId="firstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control
                        type="string"
                        placeholder="Enter first name"
                        value={firstName}
                        onChange={(e) => setfirstName(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="lastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control
                        type="string"
                        placeholder="Enter last name"
                        value={lastName}
                        onChange={(e) => setlastName(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        autoComplete="email"
                        required
                    />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
				</Form.Text>
                </Form.Group>

                <Form.Group controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        value={password1}
                        onChange={(e) => setPassword1(e.target.value)}
                        autoComplete="new-password"
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Verify Password"
                        value={password2}
                        onChange={(e) => setPassword2(e.target.value)}
                        autoComplete="new-password"
                        required
                    />
                </Form.Group>

                {isActive ?

                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
				</Button>
                    :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                        Submit
				</Button>
                }
                <Form.Text className="text-muted">
                    Already registered? <Link href="/login"><a>Please login.. </a></Link>
                </Form.Text>

            </Form>
        </React.Fragment>
    )
}