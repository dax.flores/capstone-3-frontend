import React, { useState, useEffect } from 'react';
import { Form } from 'react-bootstrap';
import AppHelper from '../app-helper'



export const GetCategory = () => {

    const [getCategoryList, setGetCategoryList] = useState([])
    const [category, setCategory] = useState('')

    useEffect(() => {

        fetch(`${AppHelper.API_URL}/categories`, {
            method: 'GET',
            headers: {
                "Content-Type": "Application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(AppHelper.toJSON)
            .then(data => {
                setGetCategoryList(data)
            })
    }, [])


    const categorySelection = getCategoryList.map((el,index) => {
        return (  
            <option key={index} value={el.category}>{el.category}</option>     
        )
    })

    function handleSelect(e) {
        setCategory(e.target.value)
        console.log(category);
    }
   

    return (
            <>
                    <Form.Group controlId="categoryLists">
                        <Form.Label>Select Category</Form.Label>
                            <Form.Control as="select" value={category} onChange={handleSelect} >
                            <option>     </option>
                            {categorySelection}
                        </Form.Control>
                    </Form.Group>
            </>
    )  
}