import React, { useState, useEffect } from 'react';
import { Container, Form, Button, Table, Row, Col, Dropdown } from 'react-bootstrap';
import moment from 'moment';
import AppHelper from '../app-helper';


export const Records = () => {

    const [user, setUser] = useState('');
    const [transactions, setTransactions] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [checkedFilter, setCheckedFilter] = useState("");

    useEffect(() => {
        fetch(`${AppHelper.API_URL}/users/details`, {
            method: 'GET',
            headers: {
                "Content-Type": "Application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(AppHelper.toJSON)
            .then(data => {
                if (data !== null) {
                    setUser(data)
                    setTransactions(data.transactions);
                }
            })
    },[])

    const clearSearch = (e) => {
        setStartDate('')
        setEndDate('')
        setCheckedFilter(false)
    }

    

   const transactionRecords = transactions.map((record,index) => {
     /*   console.log(moment(startDate).format("MMMM DD, YYYY"));
       console.log(moment(endDate).format("MMMM DD, YYYY"));
       console.log(record.date) */
       if (!startDate && !endDate && !checkedFilter && !searchTerm) {
           
           return (
               <tr key={index}>
                   <td>{record.type}</td>
                   <td>{record.category}</td>
                   <td>{record.date}</td>
                   <td>{record.text}</td>
                   <td>Php {record.amount}</td>
               </tr>
           )
          

       } else if (
        
           ((searchTerm.toLowerCase().includes(record.text.toLowerCase()) ||
               searchTerm.toLowerCase().includes(record.category.toLowerCase()) ||
               searchTerm.toLowerCase().includes(record.type.toLowerCase()))
               ||
           checkedFilter.includes(record.type)) ||
            
            ((moment(startDate) >= moment(record.date) || moment(endDate) <= moment(record.date)) &&
           (checkedFilter == record.type) && 
           searchTerm.toLowerCase().includes(record.text.toLowerCase()) &&
           searchTerm.toLowerCase().includes(record.category.toLowerCase()) &&
           searchTerm.toLowerCase().includes(record.type.toLowerCase()) )
       )
       {    
          
           return (
               <tr key={index}>
                   <td>{record.type}</td>
                   <td>{record.category}</td>
                   <td>{record.date}</td>
                   <td>{record.text}</td>
                   <td>Php {record.amount}</td>
               </tr>
           )

       } 
    })


    
  

  

    return (
        <React.Fragment>
        <Container>              
            <Form.Group controlId="text">
                <Form.Label>Search records:</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Search.."
                    onChange={(e) => setSearchTerm(e.target.value)}
                />
            </Form.Group>
            <input type="checkbox" value="" name="typeButton" onChange={(e) => clearSearch(e)} /> Both
            <input type="checkbox" value="Income" name="typeButton" onChange={(e) => setCheckedFilter(e.target.value)}/> Income
            <input type="checkbox" value="Expense" name="typeButton" onChange={(e) => setCheckedFilter(e.target.value)}/> Expense

                <Row>
                <Col>
                <Form.Group controlId="startDate">
                    <Form.Label>Start Date</Form.Label>
                    <Form.Control
                        type="date"
                        placeholder="Enter start date"
                        value={startDate}
                        onChange={(e) => setStartDate(e.target.value)}
                        required
                    />
                </Form.Group>
                    <input type="checkbox" value="" name="typeButton" onChange={(e) => clearSearch(e)} /> Both
            <input type="checkbox" value="Income" name="typeButton" onChange={(e) => setCheckedFilter(e.target.value)} /> Income
            <input type="checkbox" value="Expense" name="typeButton" onChange={(e) => setCheckedFilter(e.target.value)} /> Expense
                </Col>
                <Col>
                <Form.Group controlId="endDate" >
                    <Form.Label>End Date</Form.Label>
                    <Form.Control
                        type="date"
                        placeholder="Enter end date"
                        value={endDate}
                        onChange={(e) => setEndDate(e.target.value)}
                        required
                    />
                </Form.Group>  
                </Col>
                <Button variant="secondary" type="submit" id="submitBtn"  className="text-center justify-content-center my-4" onClick={(e)=>clearSearch(e)}>
                    Clear all filters
                </Button>
               </Row>

        <Table striped bordered hover className="text-center">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Category</th>
                        <th>Date</th>
                        <th>Text</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    {transactionRecords}
                </tbody>
        </Table>
        </Container>
        </React.Fragment>
    )
}