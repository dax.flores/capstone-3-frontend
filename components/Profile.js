import React, { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
} from "reactstrap";
import Router from 'next/router';
import Head from 'next/head';
import AppHelper from '../app-helper'

import { GlobalContext } from '../context/GlobalState';


export const Profile = () => {
    

    const [user, setUser] = useState('')

    useEffect(() => {
        fetch(`${AppHelper.API_URL}/users/details`, {
            method: 'GET',
            headers: {
                "Content-Type": "Application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(AppHelper.toJSON)
            .then(data => {
                if (data !== null) {
                    setUser(data);
                }
            })
    },[])
    
    return (
        <React.Fragment>
            { <div>
                <span className="mask bg-gradient-default opacity-8" />
                <Container className="box align-items-center" fluid>
                    <Row>
                        <Col lg="9" md="5">
                            <h1 className="display-3">Hello, {user.firstName}!</h1>
                            <p className="mt-0 mb-5">
                                This is your dashboard.
                            </p>
                        </Col>
                    </Row>
                </Container>
            </div>
            }
        </React.Fragment>
    )
}