import { Jumbotron} from 'react-bootstrap';

export default function Banner(){
	return(
		<Jumbotron >
			<h1>PeraM</h1>
			<p>Manage your money!</p>
		</Jumbotron>

	)
}