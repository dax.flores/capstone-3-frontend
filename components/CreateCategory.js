import { useState } from 'react';
import { Container, Form, Row, Col } from 'react-bootstrap';
import Router from 'next/router';
import Button from 'react-bootstrap/Button';
import AppHelper from '../app-helper'

export const CreateCategory = () => {
   
    const [categoryNew, setCategoryNew] = useState('');
    const [typeAuto, setTypeAuto] = useState('');

    function createCategory(e) {
        e.preventDefault();
        

            const options = {
                method: 'POST',
                headers: {
                    "Content-Type": "Application/json",
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    category: categoryNew,
                })
            }

            fetch(`${AppHelper.API_URL}/categories`, options)
                .then(AppHelper.toJSON)
                .then(data => {
                    // console.log(data);
                    if (data === true) {

                        alert("New category added succesfully!");
                        console.log("Success!")

                    } else {

                        alert("Something went wrong.")
                    }
                })
        setCategoryNew('');
        Router.push('/dashboard')
        }


    

    return (
        <Container fluid>
            <Row>
            <Form className="col-lg-12" onSubmit={(e) => createCategory(e)}>
            <Form.Group controlId="categoryNew">
                <Form.Label>Category:</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter new category"
                    value={categoryNew}
                    onChange={e => setCategoryNew(e.target.value)}
                    required
                />
            </Form.Group>

           
            <Button variant="primary" type="submit">
                Add Category
		    </Button>
        </Form>
        </Row>
    </Container>
    )
}