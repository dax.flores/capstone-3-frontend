import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap'
import AppHelper from '../app-helper'
import { Doughnut, Line, Bar, } from 'react-chartjs-2'
import moment from 'moment';

export const RecordCards = () => {

    const [user, setUser] = useState('')
    const [transactions, setTransactions] = useState([])
    const [records, setRecords] = useState(0)
    const [filter, setFilter] = useState('')
    const [months, setMonths] = useState([]);
    const [dailyBudget, setDailyBudget] = useState([]);
    const [monthlyBudget, setMonthlyBudget] = useState([]);


    useEffect(() => {
        fetch(`${AppHelper.API_URL}/users/details`, {
            method: 'GET',
            headers: {
                "Content-Type": "Application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(AppHelper.toJSON)
            .then(data => {
                if (data !== null) {
                    setUser(data)
                    setTransactions(data.transactions);
                    
                }
            })
    },[])
    
    useEffect(() => {
        if (transactions.length > 0) {
            let tempMonths = [];

            transactions.forEach(element => {
                if(!tempMonths.find(month => month === moment(element.date).format('MMMM'))){
                    tempMonths.push(moment(element.date).format('MMMM'))
                }
            })

            const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

            tempMonths.sort((a,b) => {
                if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1) {
                    return monthsRef.indexOf(a) - monthsRef.indexOf(b);
                }
            })
  /*           console.log(tempMonths); */
            setMonths(tempMonths);
        }
    }, [transactions])

    useEffect(() => {

        setMonthlyBudget(months.map(month => {
            let amount = 0;

            transactions.forEach(element => {
                if(month === moment(element.date).format('MMMM')){
                    amount += element.amount;
                   /*  console.log(element.amount) */
                }
            })
            return amount;
            
        }))
    }, [months])
    
/*     console.log(monthlyBudget); */


    function moneyFormatter(num) {
        let p = num.toFixed(2).split('.');
        return (
            'PHP ' +
            p[0]
                .split('')
                .reverse()
                .reduce(function (acc, num, i, orig) {
                    return num === '-' ? acc : num + (i && !(i % 3) ? ',' : '') + acc;
                }, '') +
            '.' +
            p[1]
        );
    }

    const transactionList = transactions.map(transaction => transaction.amount);

    const income = transactionList
        .filter(item => item > 0)
        .reduce((acc, item) => (acc += item), 0);

    const expense = (
        transactionList.filter(item => item < 0).reduce((acc, item) => (acc += item), 0) *
        -1
    );
    const total = income - expense;
    
   /*  console.log(transactionList);
    console.log(income);
    console.log(expense);
    console.log(total); */

    const doughnutChartSettings = {
        datasets: [{
            data: [
                income,
                expense
            ],
            backgroundColor: [
                "blue",
                "orange"
            ]
        }],
        labels: [
            "Income",
            "Expense"
        ]
    }

    const barChartSettings = {
        labels: months,
        datasets: [
            {
            label: 'Budget Status',
            backgroundColor: 'blue',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'orange',
            hoverBordercolor: 'rgba(255,99,132,1)',
            data: monthlyBudget
            }
        ]
    }

    const lineChartSettings = {
        labels: months,
        datasets: [
            {
                label: 'Monthly Balance',
                backgroundColor: 'blue',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'orange',
                hoverBordercolor: 'rgba(255,99,132,1)',
                data: monthlyBudget
            }
        ]
    }
    
    return (
    <React.Fragment>
        <Container fluid>
            <Row>

            <Col className="col-4">
                <Doughnut data={doughnutChartSettings} redraw={false} />
            </Col>
            <Col className="col-4">
                <Bar data={barChartSettings} redraw={false} />
            </Col>
            <Col className="col-4">
                <Line data={lineChartSettings} redraw={false} />
            </Col>

            </Row>
        </Container>
            
    </React.Fragment>
    )
}