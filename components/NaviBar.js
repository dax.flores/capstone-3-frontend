import React, {useState, useContext} from 'react';
import Link from 'next/link';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';


import { ThemeProvider } from "styled-components";
import { GlobalStyles } from "./GlobalStyles";
import { lightTheme, darkTheme } from "./Themes"
import { Switch } from "antd"
import 'antd/dist/antd.css';


export default function NaviBar() {

	const {user} = useContext(UserContext);

	const [theme, setTheme] = useState('light');
	const themeToggler = () => {
		theme === 'light' ? setTheme('dark') : setTheme('light')
	}




	return (
		<Navbar className="color-nav" variant="light" expand="lg" >		
	
			<Link href="/">
				<a className="navbar-brand">PeraM</a>
			</Link>

			<ThemeProvider theme={theme === 'light' ? lightTheme : darkTheme}>
				<>
					<GlobalStyles />
					
						<Switch onClick={themeToggler} />

						{
							theme === 'light' ?

								<span>Light Mode</span>
								:
								<span>Dark Mode</span>

						}
				</>
			</ThemeProvider>


		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
			{(user.token !== null) ?
					<React.Fragment>
					<Link href="/dashboard">
						<a className="nav-link" role="button">Dashboard</a>
					</Link>
					<Link href="/records">
						<a className="nav-link" role="button">Records</a>
					</Link>
					<Link href="/profile">
						<a className="nav-link" role="button">Profile</a>
					</Link>
		      		<Link href="/logout">
						<a className="nav-link" role="button">Logout</a>
					</Link>
					</React.Fragment>
		      	:
		      		<React.Fragment>
					<Link href="/">
						<a className="nav-link" role="button">Home</a>
					</Link>
		      	     <Link href="/login">
		      			<a className="nav-link" role="button">Login</a>
		      		</Link>
		      		 <Link href="/register">
		      				<a className="nav-link" role="button">Register</a>
		      		</Link>		      	
		      		</React.Fragment>
		      }
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
	)
}