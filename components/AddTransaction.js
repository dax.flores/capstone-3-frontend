import React, { useState, useEffect } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import { Container, Row, Col, Form, Button} from 'react-bootstrap';
import moment from 'moment';
import AppHelper from '../app-helper'

export const AddTransaction = () => {
  const [text, setText] = useState('');
  const [type, setType] = useState('');
  const [category, setCategory] = useState('');
  const [date, setDate] = useState('');
  const [amount, setAmount] = useState('');
  const [toggle, setToggle] = useState(null)

  const onSubmit = e => {
    e.preventDefault();
    
    const options = {
      method: 'POST',
      headers: { 
        "Content-Type": "Application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        text: text,
        category: category,
        type: type,
        amount: amount,
        date: moment(date).format("MMMM DD, YYYY"),
      })
    }

    fetch(`${AppHelper.API_URL}/users/transactions`, options)
    .then(AppHelper.toJSON)
    .then(data => {
      if (data === true) {

        alert("Transaction added succesfully!");
        console.log("Success!")

      } else {

        alert("Something went wrong.")
      }
    })
    setText("");
    setCategory("");
    setDate("");
    setAmount("");
    setType("");
    setToggle(false)
    Router.push('/dashboard')
  }

  /* const toggler = () => {
    if (toggle == null) {
      setToggle(false)
      setType("Income")
    } else {
      setToggle(true)
      setType("Expense")
    } 
  } */

  

    const [getCategoryList, setGetCategoryList] = useState([])

    useEffect(() => {

      fetch(`${AppHelper.API_URL}/categories`, {
        method: 'GET',
        headers: {
          "Content-Type": "Application/json",
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then(AppHelper.toJSON)
        .then(data => {
          setGetCategoryList(data)
        })
    }, [])


    const categorySelection = getCategoryList.map((el, index) => {
      return (
        <option key={index} value={el.category}>{el.category}</option>
      )
    })

    function handleSelect(e) {
      setCategory(e.target.value)
      console.log(category);
    }
    

  return (
    <React.Fragment>
      <Container fluid>
      <Row>

          <Form.Group controlId="categoryLists">
            <Form.Label>Select Category</Form.Label>
            <Form.Control as="select" value={category} onChange={handleSelect} >
              <option>     </option>
              {categorySelection}
            </Form.Control>
          </Form.Group>

        <Form className="col-lg-12"  onSubmit={(e) => onSubmit(e)}>
              
       

        <Row>
        <Col>
              <input type="radio" value="Income" name="typeButton" onClick={(e) => setType(e.target.value)} /> Income

        </Col>
       
        <Col>
              <input type="radio" value="Expense" name="typeButton" onClick={(e) => setType(e.target.value)} /> Expense

        </Col>
        </Row>
    
                <Form.Group controlId="amount">
                  <Form.Label>Amount</Form.Label>
                  {type === "Income" ?
                  <Form.Control
                    type="number"
                    placeholder="Enter amount"
                    value={amount}
                    onChange={(e) => setAmount(e.target.value)}
                    required
                  />
                  :
                    <Form.Control
                      type="number"
                      placeholder="Enter amount"
                      value={amount * -1}
                      onChange={(e) => setAmount(e.target.value)}
                      required
                    />
                  }
                </Form.Group>
        
          
         
 

        <Form.Group controlId="text">
          <Form.Label>Details</Form.Label>
          <Form.Control
            type="text"
            as="textarea"
            rows={2}
            placeholder="Enter details"
            value={text}
            onChange={(e) => setText(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="date">
                <Form.Label>Date</Form.Label>
                <Form.Control
                  type="date"
                  placeholder="Enter date"
                  value={date}
                  onChange={(e) => setDate(e.target.value)}
                  required
                />
        </Form.Group>

        <Button variant="primary" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center">
                  Add Transaction
        </Button>
	    </Form>
      </Row>
    </Container>
  </React.Fragment>
  )
}
