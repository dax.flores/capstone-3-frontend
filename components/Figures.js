import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardTitle, Container, Row, Col } from "reactstrap";
import AppHelper from '../app-helper'



export const Figures = () => {

    const [user, setUser] = useState('')
    const [transactions, setTransactions] = useState([])


    useEffect(() => {
        fetch(`${AppHelper.API_URL}/users/details`, {
            method: 'GET',
            headers: {
                "Content-Type": "Application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(AppHelper.toJSON)
            .then(data => {
                if (data !== null) {
                   /*  console.log("success!");
                    console.log(data);
                    console.log(data.transactions); */
                    setUser(data)
                    setTransactions(data.transactions);
                    return user
                }
            })
    },[])
    
 /*    console.log(user);
    console.log(transactions); */

    function moneyFormatter(num) {
        let p = num.toFixed(2).split('.');
        return (
            'PHP ' +
            p[0]
                .split('')
                .reverse()
                .reduce(function (acc, num, i, orig) {
                    return num === '-' ? acc : num + (i && !(i % 3) ? ',' : '') + acc;
                }, '') +
            '.' +
            p[1]
        );
    }

    const amounts = transactions.map(transaction => transaction.amount);

    const income = amounts
        .filter(item => item > 0)
        .reduce((acc, item) => (acc += item), 0);

    const expense = (
        amounts.filter(item => item < 0).reduce((acc, item) => (acc += item), 0) *
        -1
    );
    
    const total = income - expense
    
    
    return (
        <React.Fragment>
            <Container fluid>
                <div className="header-body">
                    <Row>
                        <Col lg="4">
                            <Card className="card-stats mb-4 mb-xl-0">
                                <CardBody>
                                    <Row>
                                        <div className="col">
                                            <CardTitle
                                                tag="h5"
                                                className="text-uppercase text-muted mb-0"
                                            >
                                                Balance
                                            </CardTitle>
                                            <span className="h2 font-weight-bold mb-0">
                                                PHP {total}
                                            </span>
                                        </div>

                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col lg="4">
                            <Card className="card-stats mb-4 mb-xl-0">
                                <CardBody>
                                    <Row>
                                        <div className="col">
                                            <CardTitle
                                                tag="h5"
                                                className="text-uppercase text-muted mb-0"
                                            >
                                                Income
                                            </CardTitle>
                                            <span className="h2 font-weight-bold mb-0">{moneyFormatter(income)}</span>
                                        </div>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col lg="4">
                            <Card className="card-stats mb-4 mb-xl-0">
                                <CardBody>
                                    <Row>
                                        <div className="col">
                                            <CardTitle
                                                tag="h5"
                                                className="text-uppercase text-muted mb-0"
                                            >
                                                Expense
                                            </CardTitle>
                                            <span className="h2 font-weight-bold mb-0">{moneyFormatter(expense)}</span>
                                        </div>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </Container>
        </React.Fragment>
    )
}